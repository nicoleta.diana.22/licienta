export const servicesData=[
    {
        title:'Makeup',
        image: require('../../assets/categories/makeup.jpg')
    },
    {
        title:'Barber',
        image: require('../../assets/categories/barber.jpg')
    },
    {
        title:'Nails',
        image: require('../../assets/categories/nails.jpg')
    },
    {
        title:'Hair styles',
        image: require('../../assets/categories/womenHair.jpg')
    },
]