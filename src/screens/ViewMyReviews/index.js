import { View, Text,FlatList} from 'react-native'
import React,{useEffect,useState,useContext}from 'react'
import MyReviewComponent from '../../components/MyReviewComponent'
import firestore from '@react-native-firebase/firestore';
import { AuthContext } from '../../navigation/AuthProvider';
import {Button} from 'react-native-paper'
import {useNavigation} from '@react-navigation/native';
const index = ({route,navigation}) => {
    const {user} = useContext(AuthContext);
    const[reviews,setReviews]=useState(null)
    const [loading, setLoading]=useState(true)
    const[deleted, setDeleted]=useState()
    useEffect(() =>{
        const fetchReviews=async() =>{
            try {
                const list =[];
           await firestore()
           .collection('reviews').where('userId', '==', route.params ? route.params.userId : user.uid).get()
          .then((querySnapshot)=>{
          querySnapshot.forEach(doc=>{
              const {img, comment,name}=doc.data();
              list.push({
                  id:doc.id,
                  img, comment,name
                   })
                })
              })
          setReviews(list);
    
          if(loading){
              setLoading(false);
          }
          console.log('reviews',list)
            }
            catch (e) {
                console.log(e);
            }
        }
        fetchReviews();
      },[]);
  ;
 const EmptyListMessage=({item})=>{
     return (
         <View style={{alignItems:'center',backgroundColor:'#fff6ff'}}>
             <Text style={{fontSize:16,margin:50,color:"#8A2BE2"}}>Nu aveţi recenzii.</Text>
             <Button icon="map" onPress={() => navigation.navigate('Exploreaza')}>Explorează saloanele din oraşul tau</Button>
         </View>
     )
 }
  return (
    <View style={{backgroundColor:'#fff6ff'}}>
       
       <FlatList
       data={reviews}
       renderItem={({item})=>
           
               
                        <MyReviewComponent item={item} />
                  
        
      }
      keyExtractor={(item) => item.id}
      ListEmptyComponent={EmptyListMessage}
       />
    </View>
  )
}

export default index