import React, { useEffect,useState } from "react"
import { SafeAreaView,  StyleSheet,View,Text} from "react-native"
import MapView, { Marker,PROVIDER_GOOGLE } from "react-native-maps"
import { check, request, PERMISSIONS, RESULTS } from "react-native-permissions"
import Geolocation from "react-native-geolocation-service"
import {customStyleMap} from '../../styles/mapstyles/index'
import {StatusBar, Platform} from 'react-native'; 
import styled from 'styled-components/native'; 
import MapViewDirections from 'react-native-maps-directions';
const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
`;
const mapContainer = {
  flex: 3,
};
const index= ({route, navigation}) => {
  const {latitude}=route.params
  const {longitude}=route.params
  const {address}=route.params
  const {name}=route.params
  const {city}=route.params
  const [location, setLocation] = useState(null)
  const handleLocationPermission = async () => { 
    let permissionCheck = '';
    if (Platform.OS === 'android') {
      permissionCheck = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);

      if (
        permissionCheck === RESULTS.BLOCKED ||
        permissionCheck === RESULTS.DENIED
      ) {
        const permissionRequest = await request(
          PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        );
        permissionRequest === RESULTS.GRANTED
          ? console.warn('Location permission granted.')
          : console.warn('location permission denied.');
      }
    }
  };
  useEffect(() => { 
    Geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords
        setLocation({ latitude, longitude })
      },
      error => {
        console.log(error.code, error.message)
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    )
  }, [])

  useEffect(() => {
    handleLocationPermission()
  }, [])
  // const destination = {latitude: Number(latitude), longitude: Number(longitude)};
  // const origin = {latitude: 37.3318456, longitude: -122.0296002};
  const origin = {latitude: 37.3318456, longitude: -122.0296002};
const destination = {latitude:Number(latitude), longitude:Number(longitude)};
  const GOOGLE_MAPS_APIKEY = 'AIzaSyBiDWvevOk6DvTST09rvZG5WQbVzAhazjw';
  return (
    <SafeAreaView style={styles.container}>
    <StatusBar barStyle="dark-content" />
    {location && ( 
      <MapView
         style={styles.map}
        provider={PROVIDER_GOOGLE}
        
        initialRegion={{
          latitude: latitude,  
          longitude: longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
         }}
        customMapStyle={customStyleMap} 
        // paddingAdjustmentBehavior="automatic" 
         showsMyLocationButton={true} 
         showsBuildings={true} 
         maxZoomLevel={17.5} 
        loadingEnabled={true} 
        loadingIndicatorColor="#fcb103" 
        // loadingBackgroundColor="#242f3e" 
        showsUserLocation={true}
      >
       
       <MapViewDirections
       origin={origin}
       destination={destination}
       apikey={GOOGLE_MAPS_APIKEY}
       strokeWidth={3}
       strokeColor="hotpink"
       optimizeWaypoints={true}
        />
            <Marker 
           coordinate={{
            latitude: latitude && Number(latitude) ? Number(latitude) :0,
            longitude: longitude && Number(longitude) ? Number(longitude) :0
           }}
           title={name}
         
         
         /> 
      </MapView>
      
    )}
   <View style={styles.address}>
     <Text style={styles.addressText}>{address}, {city}</Text>
     </View> 
  </SafeAreaView>

  )
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    alignItems: "center",
    height:'100%'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  address:{
    backgroundColor:'#D8BFD8',
    padding:20,
    margin:10,
    borderRadius:20,
  

  },
  addressText:{
    fontWeight:'bold',
  

  }
})

export default index