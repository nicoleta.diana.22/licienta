import React, {useContext, useState} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet,Modal} from 'react-native';
import FormInput from '../../components/FormInput';
import FormButton from '../../components/FormButton';
import { AuthContext } from '../../navigation/AuthProvider';
import SocialButton from '../../components/SocialButton';

const ForgotPassword = ({navigation}) => {
  const [email, setEmail] = useState();
  const {passwordReset} = useContext(AuthContext);
  const [show,setShow]=useState(false)
  const [modalVisible, setModalVisible] = useState(false);
  const showModal = () => {
    setModalVisible(true);
    setTimeout(() => {
      setModalVisible(false);
    }, 5000);
  };
  const handlePassword=()=>{
    passwordReset(email)
    showModal();
    setTimeout(() => {
      navigation.navigate('Login');
      }, 3000);
  }
  return (
    
    <View style={styles.container}>
       <Image
        source={require('../../../assets/images/forgot.jpg')}
        style={styles.logo}
      />
      <Text style={styles.text}>Introdu adresa de email.</Text>
      <FormInput
        labelValue={email}
        onChangeText={(userEmail) => setEmail(userEmail)}
        placeholderText="Email"
        iconType="user"
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
      />
       <FormButton
                  buttonType='outline'
                  buttonTitle="Trimite email"
                  title='Send Email'
                  buttonColor='#039BE5'
                  onPress={() =>handlePassword()}
                />
                          <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View
          style={{
            
            alignItems: 'center',
            backgroundColor: '#3CB371',
            justifyContent: 'center',
            borderRadius:10,
            height:100,
            width:350,
            alignSelf:'center',
            marginTop:200,
            opacity:1
          }}>
          <Text style={{fontSize: 16, color: 'white'}}>
              Verificaţi emailul pentru a schimba parola.
          </Text>
        </View>
      </Modal>
    </View>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#fff6ff',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 28,
    marginBottom: 10,
    color: '#051d5f',
  },
  navButton: {
    marginTop: 15,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
    fontFamily: 'Lato-Regular',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 35,
    justifyContent: 'center',
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: '400',
    fontFamily: 'Lato-Regular',
    color: 'grey',
  },
  logo: {
    height: 110,
    width: 150,
    resizeMode: 'cover',
    marginBottom: 10,
    marginTop: 30,
    borderRadius:10,
    
  },
});