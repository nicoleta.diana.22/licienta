import { View, Text,FlatList } from 'react-native';
import React,{useState, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';
import StilistComponent from '../../components/StilistComponent'
import { Searchbar } from 'react-native-paper';
const index = () => {
  const[staff, setStaff]=useState();
  const [loading, setLoading]=useState(true)
  const [searchQuery, setSearchQuery] = useState('');
  const onChangeSearch = query => setSearchQuery(query);
  useEffect(() =>{
    const fetchStaff=async() =>{
        try {
            const list =[];
       await firestore()
      .collection('salon').doc(`${item.id}`).collection('services')
      .get()
      .then((querySnapshot)=>{
      querySnapshot.forEach(doc=>{
          const {staffFname, staffLname}=doc.data();
          list.push({
              id:doc.id,
             staffFname
             ,staffLname
          })
      })
      })
      setStaff(list);

      if(loading){
          setLoading(false);
      }
      console.log('staff',list)
        }
        catch (e) {
            console.log(e);
        }
    }
    fetchStaff();
  },[]);
  return (
    <View>

      <FlatList
       data={staff}
       renderItem={({item})=>(
          <StilistComponent item={item}/>
      
      )}
       />
      
     
    </View>
  );
};

export default index;
