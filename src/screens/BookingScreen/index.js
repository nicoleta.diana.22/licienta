import React,{useState,useEffect,useContext} from "react";
import { View, Text,ScrollView, Image,StyleSheet,Alert,Modal} from "react-native";
import {LocaleConfig} from 'react-native-calendars';
import { Provider,Portal,Button,RadioButton,Card} from 'react-native-paper'
import {AuthContext} from '../../navigation/AuthProvider'
import firestore from '@react-native-firebase/firestore';
import CalendarStrip from 'react-native-calendar-strip';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { TextInput, DataTable } from "react-native-paper";
import { NavigationHelpersContext } from "@react-navigation/native";
import { id } from "date-fns/locale";
const index = ({navigation,route}) => {
  const {user}=useContext(AuthContext);
  const {address}=route.params
  const {city}=route.params
  const {latitude}=route.params
  const {longitude}=route.params
  const {name}=route.params
    const [programari, setProgramari]=useState(null)
   const [time, setTime]=useState(null)
  const [loading, setLoading]=useState(true)
  const [modalVisible, setModalVisible] = useState(false);
  const [value,setValue]=useState(null)
  const [deleted, setDeleted] = useState(false);
  const [show, setShow]=useState(false)
   const[day, setDay]=useState(null);
  const {item}=route.params;
  const [visible, setVisible] = useState(false);
  const containerStyle = {backgroundColor: 'white', padding: 20,margin:20};
  const [orar, setOrar]=useState(null);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const[selectedDate, setSelectedDate]=useState(null)
  const [selectedHour, setSelectedHour]=useState(null);
  const [phone, setPhone]=useState(null)
  const [numePers, setNumePers]=useState(null)
  const [disable, setDisable]=useState(false)
  const showModalConfirm = () => {
    setModalVisible(true);
    setTimeout(() => {
      setModalVisible(false);
    }, 5000);
  };

  const datesBlacklistFunc = date => {
    return date.isoWeekday() === 7 ;//dezactiveaza duminica
  
  }
  const save=()=>{
      
      firestore().collection('programari').add({
                          userId:user.uid, 
                           hour:value,
                            serviceid:item.id,
                           service:item.service,
                           price:item.price,
                           staffName:item.staffFname,
                           staffLName:item.staffLname,
                           date:selectedDate,
                           hour:selectedHour,
                           address:address,
                           city:city,
                           latitude:latitude,
                           longitude:longitude,
                           name:name
                           
                 }).then(() => {
                            
                            console.log('V-ati programat');
                            
                            showModalConfirm()
                            setTimeout(() => {
                              navigation.navigate('Activity');
                              }, 4000);
                          })
                             .catch((error)=>{ console.log('Not ok!')
                                               })

  
                                              }
   const onDateSelected =(selectedDate,date) => {
    setSelectedDate( selectedDate.format('MM/DD/YY'))
   
       setShow(true)
       
    }
    const handleSave = () => {
      Alert.alert(
    'Programează-te',
    'Finalizezi?',
    [
      {
        text: 'Renunţă',
        onPress: () => console.log('Renuntat!'),
        style: 'Anulează',
      },
      {
        text: 'Confirmă',
        onPress: () => save(),
       
      },
    ],
    {cancelable: false},
  );
};
   const markedDatesFunc = date => {
      // Dot
      if ((date.isoWeekday() === 4) || (date.isoWeekday()==1) ){
        return {
          lines:[{
            color:	"#008000" ,
            selectedColor: "#32CD32",
          }]
        };
      }
      
    }     
    useEffect(() =>{
          const fetchProgramari=async() =>{
          try {
              const list =[];
         await firestore()
        .collection('programari').where('serviceid','==',item.id)
        .get()
        .then((querySnapshot)=>{
        querySnapshot.forEach(doc=>{
            const {hour, date, serviceId}=doc.data();
            list.push({
                id:doc.id,
              hour, date, service:serviceId
            })
        })
        })
        setProgramari(list);
        console.log('programari',list)
       
     
          }
          catch (e) {
              console.log(e);
          }
      }
      fetchProgramari();
    },[]);

   
               
  return (
  <View style={{backgroundColor:'#fff6ff'}}>
    <ScrollView>
      <View style={{margin:25}}>
      <Image  source={{uri: item.img}}  style={styles.image}  />     
     <Text style={styles.text} >{item.service} : {item.price} RON</Text>
     </View>
     <Text style={styles.title}>Alege o data</Text>
     <View style={styles.containerCalendar}>
     <CalendarStrip
     
      scrollable
      style={{height:160, paddingTop: 12, paddingBottom: 12}}
      calendarColor={'#D8BFD8'}
      calendarHeaderStyle={{color: '#BA55D3'}}
      dateNumberStyle={{color: '#BA55D3'}}
      dateNameStyle={{color: '#BA55D3'}}
      iconContainer={{flex: 0.1}}
      numDaysInWeek={7}
      scrollerPaging={false}
      startingDate={new Date()}
      onDateSelected={onDateSelected}
      datesBlacklist={datesBlacklistFunc}
      showDayNumber={true}
      showDayName={true}
      useIsoWeekday={true}
      // markedDates={markedDatesFunc}
      minDate={new Date()}
      calendarHeaderContainerStyle={{margin:10}}
    
    />
    </View>
    
     <View>
     {
       show &&(
         
        <View style={styles.containerTitle}>
            <Text style={styles.title}> Alege ora pentru data de {selectedDate} </Text>
        
                           {item.program.map(
                             element=>
                      {
                        if(element.data===moment(selectedDate).isoWeekday()){
                        
                        return (
                          <RadioButton.Group value={selectedHour} onValueChange={(itemValue, itemIndex) => setSelectedHour(itemValue)    }>
                             { element.hours.map(hour => {
                                  const isBusy = programari.find(prog => prog.date === selectedDate && prog.hour ===hour);

                                  if (isBusy) {
                                    return(
                                      <RadioButton.Item label={hour} value={hour} disabled={true} uncheckedColor='red' style={{backgroundColor:'#FF9999',opacity:0.3}}/>
                                     ) 
                                  } else {
                                    return ( <RadioButton.Item label={hour} value={hour} disabled={false} uncheckedColor='green' style={{backgroundColor:'#00ff80',opacity:0.3}}/>)
                                  }                              
                               })}
                              
                           </RadioButton.Group>
                      
                        )
                      }
                    }
                  
                           )}     
                        
        <Button onPress={handleSave}>Programează-te</Button>
        {/* <Card style={styles.ocupat}>
        <Text>Orele ocupate in data de {selectedDate}</Text>
          {programari.map(program=>
          {if(selectedDate==program.date){
            return(
            
              
              <DataTable>
        
              <DataTable.Row>
                <DataTable.Cell style={styles.cell}>{program.hour}</DataTable.Cell>
              </DataTable.Row>
            </DataTable>
           
            )
          }}
              
           ) }
        </Card> */}
      </View>
       
       )
     }
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View
          style={{
            alignItems: 'center',
            backgroundColor: '#3CB371',
            justifyContent: 'center',
            borderRadius:10,
            height:100,
            width:350,
            alignSelf:'center',
            marginTop:200,
            opacity:1
          }}>
          <Text style={{fontSize: 16, color: 'white'}}>
            Programarea s-a înregistrat cu succes!
          </Text>
        </View>
      </Modal>
    </View>
    </View>
    
      </ScrollView>
</View>
  )
} 
export default index;

const styles=StyleSheet.create({
   image:{ width: '95%',
    // aspectRatio:2,
    resizeMode: 'cover',
    borderRadius: 10,
    padding:20,
    margin:10,
    height:180
},
text:{
   fontSize:25,
   fontWeight:"bold",
   padding:5,
   margin:5
},
serv:{
 
},
textprice:{
  fontSize:16,
  fontWeight:"bold",
  padding:5,  margin:5}
    ,
ocupat:{
   backgroundColor:"#DC143C",
   marginTop:90,
   marginBottom:0,
   marginLeft:80,
   marginRight:80

    
    
},
title:{
  fontSize:20,
  color:'#DA70D6',
  alignContent:"center",
  alignSelf:'center'
},
containerTitle:{
  margin:12,

},
containerCalendar:{
  margin:12
}})

