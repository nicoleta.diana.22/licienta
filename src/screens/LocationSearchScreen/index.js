import React, {useState} from "react";
import { View, TextInput, Text, FlatList, Pressable } from "react-native";
import styles from './styles.js';
import {useNavigation} from '@react-navigation/native';
import SuggestionRow from "./SuggestionRow";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import Entypo from 'react-native-vector-icons/Entypo'
const LocationSearchScreen = (props) => {
   const[inputText, setInputText] =useState('');
  const navigation = useNavigation();


  return (
    <View style={styles.container}>
      <GooglePlacesAutocomplete
        placeholder='Tasteaza un oras'
        onPress={(data, details = null) => {
          console.log(data, details);
          navigation.navigate('Date', { viewport: details.geometry.viewport });
        }}
        fetchDetails
        styles={{
          textInput: styles.textInput,
        }}
        query={{
          key: 'AIzaSyB_nWN_2VGA4vtP0RDpHtanTCQVIY8uYAI',
          language: 'ro',
          types: '(cities)',
        }}
        suppressDefaultStyles
        renderRow={(item) => <SuggestionRow item={item} />}
      />
    </View>
  );
};

export default LocationSearchScreen;