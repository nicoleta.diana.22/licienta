import { View, Text,FlatList} from 'react-native';
import React,{useState,useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';
import Salon from '../../components/Salon'
const index = () => {
  
    const [services,setServices]=useState(null)
    const [loading, setLoading]=useState(true)
    const text="Unghii"
  
    const onChangeSearch = query => setSearchQuery(query);
    useEffect(() =>{
      const fetchServices=async() =>{
          try {
              const list =[];
         await firestore()
        .collection('services')
        .get()
        .then((querySnapshot)=>{
        querySnapshot.forEach(doc=>{
            const {img,price,service,staffName,staffName2}=doc.data();
            list.push({
                id:doc.id,
                img,price,service,staffName,staffName2
                
            })
        })
        })
        setServices(list);
  
        if(loading){
            setLoading(false);
        }
        console.log('services',list)
          }
          catch (e) {
              console.log(e);
          }
      }
      fetchServices();
    },[]);
    
  return (
    
    <View>
        
   
         <FlatList
         data={services}
         renderItem={({item})=>(
          
            <Salon item={item}/>)}
            keyExtractor={(item) => item.id}
    
       />

    </View>
  );
};

export default index;
