import { View, Text } from 'react-native'
import React,{useState, useEffect}from 'react'
import { FlatList } from 'react-native-gesture-handler';
import Post from '../../components/Post/Post/index'
import firestore from '@react-native-firebase/firestore';
import {Button} from 'react-native-paper'
const Services = ({navigation, route}) => {
    const {text}=route.params;
    const {value}=route.params;
    const [salon, setSalon]=useState(null)
    const [loading, setLoading]=useState(true)
    const [uploading, setUploading] = useState(true);
    useEffect(() =>{
        setUploading(true);
        const fetchSalon=async() =>{
            try {
                const list =[];
                setUploading(true);
           await firestore()
          .collection('salon').where('servicii','array-contains',text).where('city','==',value)
          .get()
          .then((querySnapshot)=>{
          querySnapshot.forEach(doc=>{
              const {name, address,img,desc,email,phone,points,city,servicii,latitude,longitude}=doc.data();
              list.push({
                  id:doc.id,
                  name:name,
                  address:address,
                  img,
                  desc:desc,
                  email:email,
                  phone:phone,
                  points,city,servicii,latitude,longitude
              })
          })
          })
          setSalon(list);
          setUploading(false);
          if(loading){
              setLoading(false);
          }
          console.log('salon',list)
            }
            catch (e) {
                console.log(e);
            }
        }
        fetchSalon();
      },[text,value]);
      const EmptyListMessage=({item})=>{
        return (
            <View style={{alignItems:'center',backgroundColor:'#fff6ff'}}>
                <Text style={{fontSize:16,margin:50,color:"#8A2BE2"}}>Nu sunt rezultate în urma căutării dvs.</Text>
                <Button icon="map" onPress={() => navigation.navigate('Exploreaza')}>Explorează saloanele cele mai apropiate</Button>
            </View>
        )
    }
  return (
    <View>
        <FlatList
        data={salon}
        renderItem={({item})=>(
        
         
            <Post item={item}/>
         
       )}
       keyExtractor={(item) => item.id}
       ListEmptyComponent={EmptyListMessage}
        />
      
    </View>
  )
}

export default Services