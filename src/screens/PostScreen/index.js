import React,{useState,useEffect} from "react";
import { View, Text,ScrollView, Image,StyleSheet,SafeAreaView} from "react-native";
import {Button} from 'react-native-paper'
import firestore from '@react-native-firebase/firestore';
import { FlatList } from "react-native-gesture-handler";
import { windowWidth } from '../../utils/Dimension';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Card} from 'react-native-paper'
import ServiceSalon from '../../components/ServiceSalon'
import ReviewComponent from '../../components/ReviewComponent'
import { style } from "@mui/system";
const PostScreen = ({navigation,route}) => {
  const [services,setServices]=useState(null)
  const [loading, setLoading]=useState(true)
  const [deleted, setDeleted] = useState(false);
  const [reviews,setReviews]=useState(null)
  const {item}=route.params;
  useEffect(() =>{
    const fetchServices=async() =>{
        try {
            const list =[];
       await firestore()
      .collection('salon').doc(`${item.id}`).collection('services')
      .get()
      .then((querySnapshot)=>{
      querySnapshot.forEach(doc=>{
          const {service, img ,price, staffFname,staffLname,program,address,city,latitude,longitude,name}=doc.data();
          list.push({
              id:doc.id,
               img,
               service,
               price,
               staffFname,
               staffLname,
               program,
               address:item.address,
               name:item.name,
               city:item.city, 
               latitude:item.latitude,
               longitude:item.longitude
               })
            })
          })
      setServices(list);

      if(loading){
          setLoading(false);
      }
      console.log('services',list)
        }
        catch (e) {
            console.log(e);
        }
    }
    fetchServices();
  },[]);

  useEffect(() =>{
    const fetchReviews=async() =>{
        try {
            const list =[];
       await firestore()
      .collection('reviews').where('name','==',`${item.name}`)
      .get()
      .then((querySnapshot)=>{
      querySnapshot.forEach(doc=>{
          const {img, comment,namePerson}=doc.data();
          list.push({
              id:doc.id,
              img, comment,namePerson
               })
            })
          })
      setReviews(list);

      if(loading){
          setLoading(false);
      }
      console.log('reviews',list)
        }
        catch (e) {
            console.log(e);
        }
    }
    fetchReviews();
  },[`${item.name}`]);
  const EmptyListMessage=({item})=>{
    return (
        <View style={{alignItems:'center',backgroundColor:'#fff6ff'}}>
            <Text style={{fontSize:16,margin:5,color:"#8A2BE2"}}>Nu sunt recenzii pentru acest salon.</Text>
        </View>
    )
}
const EmptyListMessage2=({item})=>{
  return (
      <View style={{alignItems:'center',backgroundColor:'#fff6ff'}}>
          <Text style={{fontSize:16,margin:50,color:"#8A2BE2"}}>Acest salon nu are servicii disponibile.</Text>
          <Button icon="map" onPress={() => navigation.navigate('Exploreaza')}>Explorează saloanele cele mai apropiate</Button>
      </View>
  )
}
  return (
    <SafeAreaView style={{flex:1, backgroundColor:'#fff6ff', height:'100%'}}>
      <ScrollView>
       <View style={styles.locationContainer}>
       <Text style={styles.location}>{item.address}</Text>
       </View>
    <View  style={{paddingLeft:20,paddingRight:20}}>
    <Text style={styles.name}>
        {item.name}
      </Text>
    <View style={styles.container}>
      {/* Image  */}
      <Image style={styles.image} source={{uri: item.img}}  />


      {/* SERVICII */}
       <View style={styles.cardView}>
       <FlatList
       data={services}
       renderItem={({item})=>(
       
          <ServiceSalon item={item} />
        
      )}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={EmptyListMessage2}
       />
      </View>
      {/* REVIEWS */}
      <View>
        <Card style={styles.reviews}>
          <Text style={styles.text}>Recenzii</Text>
          <ScrollView>
      <FlatList
       data={reviews}
       renderItem={({item})=>(
       
        
           <ReviewComponent item={item}/>
        
      )}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={EmptyListMessage}
       />
       </ScrollView>
       </Card>
      </View>
      
    </View>
    <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image source={require('../../../assets/images/desc.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 180}}>
                      <Text numberOfLines={1} style={{fontSize:14, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>Descriere</Text>
                      <Text style={{fontSize:12, fontFamily: 'Roboto-Medium', margin:6}}>{item.desc}</Text>
                     </View>
    </View>
    <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image source={require('../../../assets/images/phone.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 180}}>
                      <Text numberOfLines={1} style={{fontSize:14, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>Telefon</Text>
                      <Text style={{fontSize:12, fontFamily: 'Roboto-Medium', margin:6}}>{item.phone}</Text>
                     </View>
    </View>
    <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image source={require('../../../assets/images/email.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 180}}>
                      <Text numberOfLines={1} style={{fontSize:14, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>Email</Text>
                      <Text style={{fontSize:12, fontFamily: 'Roboto-Medium', margin:6}}>{item.email}</Text>
                     </View>
    </View>
  </View>
  </ScrollView>
  </SafeAreaView>
  );
};

export default PostScreen;

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  image: {
    width: '100%',
    aspectRatio: 3 / 2,
    resizeMode: 'cover',
    borderRadius: 10,
  },

name: {
    marginVertical: 4,
    color: '#DDA0DD',
    fontSize:20,
    padding:15,
    fontWeight:'bold'
  },
  description: {
    fontSize: 18,
    lineHeight: 26,
  },
  phone: {
    fontSize: 18,
    marginVertical: 10,
  },
  email: {
    fontSize: 18,
    marginVertical: 10,
  },
  contact: {
    fontWeight: 'bold',
  },
  totalPrice: {
    color: '#5b5b5b',
    textDecorationLine: 'underline',
  },
  longDescription: {
    marginVertical: 20,
    fontSize: 16,
    lineHeight: 24,
  },
  cardView:{
    margin:0
  },
  location:{
   marginLeft:0,
   color:'blue',
   fontSize:12

  },
  locationContainer:{
    padding:10,
    
  },
  text:{
    alignSelf:'center',
    fontSize:20,
   color:'#fce303',
  fontWeight:'bold'  },
  reviews:{
    marginTop:70,
    

  }
});
