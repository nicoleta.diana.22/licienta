import {SafeAreaView,View,Text,StyleSheet,Button} from 'react-native';
import React,{useContext,useEffect,useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {AuthContext} from '../../navigation/AuthProvider';
import Modal from 'react-native-modal';
import { TextInput } from 'react-native-paper';
const index = () => {
    const navigation=useNavigation();
    const {user, logout} = useContext(AuthContext);
    const [isModalVisible, setModalVisible] = useState(false);
    const [salon, setSalon]=useState('');
    const toggleModal = () => {
      setModalVisible(!isModalVisible);
    };
    const addSalon=()=>{
      console.log('salon adaugat!')
    }
  return (
    <View style={styles.container}>
    <View style={styles.cards}>
     <TouchableOpacity style={styles.userBtn} onPress={() => logout()}>
                <Text style={styles.userBtnTxt}>Logout</Text>
     </TouchableOpacity>
     <View style={styles.salon}>
     <TouchableOpacity onPress={toggleModal} style={styles.appButtonContainer}>
    <Text style={styles.appButtonText}>Salveaza salon</Text>
    
  </TouchableOpacity>
         </View>
         <View style={{flex: 1}}>
       
        <Modal isVisible={isModalVisible}>
          <View style={styles.modal}>
              <View style={styles.modalCont}>
              <TextInput label="Nume salon" style={styles.input} value={salon} onChangeText={salon => setSalon(salon)}/>
              </View>
            <Button title="Salveaza" onPress={toggleModal}/>
          </View>
        </Modal>
      </View>
     
    </View>

    </View>
  );
};

export default index;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  userImg: {
    height: 150,
    width: 150,
    borderRadius: 75,
  },
  userName: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10,
  },
  aboutUser: {
    fontSize: 12,
    fontWeight: '600',
    color: '#666',
    textAlign: 'center',
    marginBottom: 10,
  },
  userBtnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    marginBottom: 10,
  },
  userBtn: {
    borderColor: '#2e64e5',
    borderWidth: 2,
    borderRadius: 3,
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginHorizontal: 5,
  },
  userBtnTxt: {
    color: '#2e64e5',
  },
  userInfoWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginVertical: 20,
  },
  userInfoItem: {
    justifyContent: 'center',
  },
  userInfoTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'center',
  },
  userInfoSubTitle: {
    fontSize: 12,
    color: '#666',
    textAlign: 'center',
  },
  salon:{
    margin:20,
    padding:10
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  modal:{
    backgroundColor:'#fff',
    justifyContent:'center',
    alignItems:'center',

  },
  modalCont:{
    margin:10,
    padding:10,
    height:200
  },
  input:{
    height:50,
    margin:10,
    justifyContent:'center',
    padding:5,
    width:300
  }
});