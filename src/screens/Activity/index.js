import { View, Text, FlatList,StyleSheet,TouchableOpacity } from 'react-native'
import React,{useState,useEffect,useContext}from 'react'
import {Card,Avatar,Button,List,Colors,Alert} from 'react-native-paper'
import firestore from '@react-native-firebase/firestore';
import {AuthContext} from '../../navigation/AuthProvider'
import {useNavigation} from '@react-navigation/native'; 
import ActivityComponent from '../../components/ActivityComponent'
import { ScrollView } from 'react-native-gesture-handler';
const index = ({navigation,route}) => {
    const [programari, setProgramari]=useState(null);
    const [loading, setLoading]=useState(false)
    const {user}=useContext(AuthContext);
   
    useEffect(() =>{
        const fetchProgramari=async() =>{
            try {
                const list =[];
           await firestore()
          .collection('programari').where('userId', '==', route.params ? route.params.userId : user.uid).get()
          .then((querySnapshot)=>{
          querySnapshot.forEach(doc=>{
              const {userId,service,price,staffLName, staffName,hour,date,address,city,latitude,longitude,name}=doc.data();
              list.push({
                  userId,
                 activityId:doc.id,
                 service, hour, price,staffLName,staffName,hour, date,address,city,latitude,longitude,name
              })
          })
          })

          setProgramari(list);
    
          if(loading){
              setProgramari(false);
          }
          console.log('programari',list)
            }
            catch (e) {
                console.log(e);
            }
        }
        fetchProgramari();
      },[]);
      const EmptyListMessage=({item})=>{
        return (
            <View style={{alignItems:'center',backgroundColor:'#fff6ff'}}>
                <Text style={{fontSize:16,margin:50,color:"#8A2BE2"}}>Nu aveţi programări.</Text>
                <Button icon="map" onPress={() => navigation.navigate('Exploreaza')}>Explorează saloanele cele mai apropiate</Button>
            </View>
        )
    }
  return (
    <View style={styles.container}>
        <ScrollView>
   <FlatList
       data={programari}
       renderItem={({item})=>(
      
           <ActivityComponent item={item}/>
         
        
      )}
      keyExtractor={(item) => item.activityId}
      ListEmptyComponent={EmptyListMessage}
       />
       </ScrollView>
        <Button style={styles.button} icon='star' onPress={()=>navigation.navigate('ViewMyReviews')}>Vezi toate recenziile tale</Button>
    </View>
  )
}

export default index

const styles=StyleSheet.create({
    container:{
      flex:1,
      margin:2,
      padding:10,
      borderRadius:20,
      backgroundColor:'#fff6ff',
    },
    button:{
        backgroundColor:'#fff0f5'
    }
   
})