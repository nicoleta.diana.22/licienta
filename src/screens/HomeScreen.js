import React, {useState, useEffect, useRef} from "react";
import { View, FlatList, useWindowDimensions,Text,StyleSheet } from "react-native";
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import PostCarousel from '../components/Post/PostCarousel'
import firestore from '@react-native-firebase/firestore';
import { ScaleFromCenterAndroid } from "@react-navigation/stack/lib/typescript/src/TransitionConfigs/TransitionPresets";
import { check, request, PERMISSIONS, RESULTS } from "react-native-permissions"
import Geolocation from "react-native-geolocation-service" 
const HomeScreen= (props) => {
    const [selectedPlaceId, setSelectedPlaceId]=useState(null);
    const flatlist=useRef();
   
    const [salon, setSalon]=useState([])
    const [loading, setLoading]=useState(true)
    const handleLocationPermission = async () => { // 👈
      let permissionCheck = '';
      if (Platform.OS === 'android') {
        permissionCheck = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
  
        if (
          permissionCheck === RESULTS.BLOCKED ||
          permissionCheck === RESULTS.DENIED
        ) {
          const permissionRequest = await request(
            PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
          );
          permissionRequest === RESULTS.GRANTED
            ? console.warn('Location permission granted.')
            : console.warn('location permission denied.');
        }
      }
    };
  
    useEffect(() => {
      handleLocationPermission()
    }, [])
  
    const onChangeSearch = query => setSearchQuery(query);
    useEffect(() =>{
      const fetchSalon=async() =>{
          try {
              const list =[];
         await firestore()
        .collection('salon')
        .get()
        .then((querySnapshot)=>{
        querySnapshot.forEach(doc=>{
            const {name, address,img,latitude,longitude,city,phone,email,desc}=doc.data();
            
            list.push({
                id:doc.id,
                name:name,
                address:address,
                img,
                latitude,longitude,phone,city,email,desc
              
            })
        })
        })
        setSalon(list);
  
        if(loading){
            setLoading(false);
        }
        console.log('salon',list)
          }
          catch (e) {
              console.log(e);
          }
      }
      fetchSalon();
    },[]);
    
  const map = useRef();
  const viewConfig = useRef({itemVisiblePercentThreshold: 70})
  const onViewChanged = useRef(({viewableItems}) => {
    if (viewableItems.length > 0) {
      const selectedPlace = viewableItems[0].item;
      setSelectedPlaceId(selectedPlace.id)
    }
  })

  const width = useWindowDimensions().width;
   useEffect(() =>{
     if(!selectedPlaceId || !flatlist){
       return ;

     }
    const index=salon.findIndex(place=>place.id ===selectedPlaceId)
     flatlist.current.scrollToIndex({index})
     const selectedPlace=salon[index];
     const region ={
       latitude:selectedPlace.latitude,
       longitude:selectedPlace.longitude,
       latitudeDelta:0.8,
       longitudeDelta:0.8
     }
     map.current.animateToRegion(region);
   }, [selectedPlaceId])
      

  return (
    <View style={{width: '100%', height: '100%'}}>
      <MapView
        ref={map}
        style={{width: '100%', height: '79%'}}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 46.770439,
          longitude: 	23.591423,
          latitudeDelta: 0.8,
          longitudeDelta: 0.8,
        }}
        showsUserLocation={true}
        paddingAdjustmentBehavior="automatic" 
          showsMyLocationButton={true} 
          showsBuildings={true} 
          maxZoomLevel={17.5} 
          loadingEnabled={true} 
          loadingIndicatorColor="#fcb103" 
          loadingBackgroundColor="#242f3e" 
      >
     {salon.map( item =>   
      <Marker 
           coordinate={{
            latitude: item && Number(item.latitude) ? Number(item.latitude) :0,
            longitude: item && Number(item.longitude) ? Number(item.longitude) :0
           }}
           title={item.name}
           description={item.address}
         
         />)}
 
      </MapView>
     <View  style={{position:'absolute', bottom:0}} >
     <FlatList
          ref={flatlist}
          data={salon}
          renderItem={({item}) => <PostCarousel item={item} />}
          horizontal
          showsHorizontalScrollIndicator={false}
          snapToInterval={width - 60}
          snapToAlignment={"center"}
          decelerationRate={"fast"}
          viewabilityConfig={viewConfig.current}
          onViewableItemsChanged={onViewChanged.current}
        /> 
     </View>
    </View>
  );
}

export default HomeScreen;

const styles=StyleSheet.create({
  image:{
    width:20,height:20
  }
})