import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
const index = () => {
  const navigation=useNavigation();
  return (
    <View style={styles.container}>
      <View>
      <Text style={styles.title}> Alege utilizator</Text>
      </View>
          <View style={styles.card}>
            <Text style={styles.text}> Autentificare clienti</Text>
              <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Login')}>
                 <Image source={require('../../../assets/images/programare.jpg')} style={styles.image}/>
              </TouchableOpacity>
            </View>

          <View style={styles.card}>
            <Text style={styles.text}>Autentificare salon</Text>
          <TouchableOpacity style={styles.button} onPress={()=>navigation.navigate('Login')}>
               <Image source={require('../../../assets/images/prestator.jpg')} style={styles.image}/>
          </TouchableOpacity>
          </View>
      </View>
  );
};

export default index;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        marginTop:0,
        backgroundColor:'#e4f2f2',
    },
    card:{
       flex:1,
       padding:10,
       alignSelf:'center',
       margin:5,
       shadowColor:'#80CBC4',
       shadowOpacity:20,
       borderColor:'#80CBC4',
       backgroundColor:'#f5fafa',
       borderWidth:1,
       borderRadius:50,
       width:300,
       height:300,
       margin:5

    },
    button:{
      borderWidth:0.2,
      alignItems:'center',
      alignContent:'center',
      position:'relative',
      justifyContent:'center',
      width:200,
      height:200,
      backgroundColor:'#fff',
      alignSelf:'center',
      borderRadius:200,
      margin:30
    },
    image :{
      borderWidth:0.2,
      alignItems:'center',
      display:'flex',
      alignContent:'center',
      position:'relative',
      justifyContent:'center',
      alignSelf:'center',
      width:200,
      height:200,
      backgroundColor:'#fff',
      borderRadius:200,
      margin:0,
      padding:10
    },
    text:{
      alignContent:'center',
      fontSize:15,
      fontWeight:'bold',
      color:'#80CBC4',
      padding:5,
      alignSelf:'center',

    },
    title:{
      alignContent:'center',
      fontSize:20,
      fontWeight:'bold',
      color:'#80CBC4',
      padding:5,
      
      alignSelf:'center',
    }
});