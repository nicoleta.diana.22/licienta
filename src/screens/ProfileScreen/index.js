import React,{useContext,useEffect,useState} from 'react'
import {View, Text,Image, FlatList} from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import FormButton from '../../components/FormButton';
import { AuthContext } from '../../navigation/AuthProvider';
import {useNavigation} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
import ProfileComponent from '../../components/ProfileComponent'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput,Button } from 'react-native-paper';
const index = ({ route}) => {
    const {user, logout} = useContext(AuthContext);
    const [users,setUsers]=useState(null)
    const [loading, setLoading]=useState(true)
    const [showEdit, setShowEdit]=useState(false)
    const [name, setName]=useState();
    const navigation=useNavigation();
    useEffect(() =>{
       
        const fetchUsers=async() =>{
            try {
                const list =[];
               
           await firestore()
          .collection('users').where('uid', '==', route.params ? route.params.userId : user.uid)
          .get()
          .then((querySnapshot)=>{
          querySnapshot.forEach(doc=>{
              const {name, email}=doc.data();
              list.push({
                  id:doc.id,
                 name:name,email:email
              })
          })
          })
          setUsers(list);
        
          if(loading){
              setLoading(false);
          }
         
          console.log('users',list)
            }
            catch (e) {
                console.log(e);
            }
        }
        fetchUsers();
      },[]);
      const open =()=> {
         setShowEdit(true)
      }

      const save=()=> {
        firestore().collection('users').where('uid', '==', route.params ? route.params.userId : user.u).update({
            name:name
        })
        .then(()=>{
            console.log('Numele a fost schimbat cu succes!')
            setShowEdit(false);
        }).catch((error)=>{
            console.log('Nu s-a putut edita!')
        })
       setShowEdit(false)
      }
    return (
       <View style={{  
        flex:1,
        margin:2,
        padding:10,
        borderRadius:20,
        backgroundColor:'#fff6ff',
       }}>
           <ScrollView style={{padding:20}}>
               <View>
           <FlatList
       data={users}
       renderItem={({item})=>(
       
        
          <ProfileComponent item={item}/>
        
      )}
      keyExtractor={(item) => item.id}
       />
       </View>
               <TouchableOpacity  onPress={open}>
               <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:8, borderRadius:20, backgroundColor:'#EEE0E5', padding:15 }}>
                   <Image source= {require('../../../assets/images/edit.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}} />
                   <Text style={{fontSize:16, fontFamily: 'Roboto-Medium', margin:10}}> Editeaza profilul</Text>
                </View>

              </TouchableOpacity>
              {showEdit&& (
                    <View>
                        <TextInput label='Nume'  onChangeText={name => setName(name)} mode='outlined' outlineColor='2E0854' style={{backgroundColor:'#fcf7f9',margin:6, borderRadius:20,padding:0}}></TextInput>
                        <Button onClick={save}>Salvează</Button>
                    </View>
                )}
              <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:8, borderRadius:20, backgroundColor:'#EEE0E5', padding:15 }}>
               <Image source= {require('../../../assets/images/edit.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}} />
                    <FormButton buttonTitle='Logout' style={{width:100, height:30}} onPress={() => logout()} />
               </View>

           </ScrollView>
           </View>
    )
}

export default index
