import React,{useState, useEffect} from "react";
import { View, FlatList,ScrollView,StyleSheet,Text} from "react-native";
import Post from '../components/Post/Post/index';
import firestore from '@react-native-firebase/firestore';
import { Searchbar,ActivityIndicator} from 'react-native-paper';
import { isEqual } from "date-fns";
import {
 StatusWrapper,
} from '../styles/AddPost';
import {Button} from 'react-native-paper'
const SearchResults = ({navigation, route}) => {
  const [salon, setSalon]=useState(null)
  const [loading, setLoading]=useState(true)
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = query => setSearchQuery(query);
  const {value}=route.params;
  const [uploading, setUploading] = useState(true);
  useEffect(() =>{
    setUploading(true);
    const fetchSalon=async() =>{
        try {
            const list =[];
            setUploading(true);
       await firestore()
      .collection('salon').where('city','==',value)
      .get()
      .then((querySnapshot)=>{
      querySnapshot.forEach(doc=>{
          const {name, address,img,desc,email,phone,points,city,latitude,longitude}=doc.data();
          list.push({
              id:doc.id,
              name:name,
              address:address,
              img,
              desc:desc,
              email:email,
              phone:phone,
              points,city,latitude,longitude
          })
      })
      })
      setSalon(list);
      setUploading(false);
      if(loading){
          setLoading(false);
      }
   
      console.log('salon',list)
        }
        catch (e) {
            console.log(e);
        }
    }
    fetchSalon();
  },[value]);
  const EmptyListMessage=({item})=>{
    return (
        <View style={{alignItems:'center',backgroundColor:'#fff6ff'}}>
            <Text style={{fontSize:16,margin:50,color:"#8A2BE2"}}>Nu sunt rezultate în urma căutării dvs.</Text>
            <Button icon="map" onPress={() => navigation.navigate('Exploreaza')}>Explorează saloanele cele mai apropiate</Button>
        </View>
    )
}
  return (
    <View style={{ backgroundColor:'#fff6ff', height:'100%'}}>
      {uploading ? (
          <StatusWrapper>
            <Text> Se cauta saloanele din zona ta</Text>
            <ActivityIndicator animating={true}  />
          </StatusWrapper>
        ) 
       :  
       <View > 
         <Text style={{fontSize:20,alignSelf:'center',margin:16, fontFamily:'Verdana',fontStyle:'italic',color:'#8A2BE2'}}>Toate saloanele din {value}</Text>
       
        <FlatList
       data={salon}
       renderItem={({item})=>(
       
        
           <Post item={item}/>
        
      )}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={EmptyListMessage}
       />
       </View>
    }

      
     
    </View>
  );
};

export default SearchResults;

const styles=StyleSheet.create({
  search:{
    margin:20,
    justifyContent:'center',
    borderRadius:20,
   
  }
})