import { View, Text,TouchableOpacity} from 'react-native';
import React,{useState}from 'react';
import { SubmitBtn, SubmitBtnText,} from '../../styles/AddPost';
import storage from '@react-native-firebase/storage'
import firestore from '@react-native-firebase/firestore';
import {Dropdown, MultiSelect} from 'react-native-element-dropdown';
import DateTimePicker from '@react-native-community/datetimepicker';
import { collection, query, getDocs } from "firebase/firestore";


const index = ({staffId}) => {
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date(1598051730000));
  const [hour, setHour]=useState([]);
  const [mode, setMode] = useState('date');
  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };
  const data = [
    {label: '8:00 ', value: '1'},
    {label: '9:00', value: '2'},
    {label: '10:00', value: '3'},
    {label: '11:00', value: '4'},
    {label: '12:00', value: '5'},
    {label: '13:00', value: '6'},
    {label: '14:00', value: '7'},
    {label: '15:00', value: '8'},
    {label: '16:00', value: '9'},
    {label: '17:00', value: '10'},
    {label: '18:00', value: '11'},
    {label: '19:00', value: '12'},
    {label: '20:00', value: '13'},
    {label: '21:00', value: '14'},
    {label: '22:00', value: '15'}
]; 
const _renderItem = item => {
  return (
  <View>
      <Text >{item.label}</Text>
  </View>
  );
};
    const addprogramm =  (staffId) =>{
        firestore()
       .collection('staff').doc(staffId).create('program')
    .add({
      // userId:user.uid,
       date:date,
       hour:hour,
    
    })
    .then(() => {
      console.log('Programm added!');
      setLname(null);
    })
    .catch((error)=>{ console.log('Ceva nu merge ok!')}) }
    const showDatepicker = () => {
      showMode('date');
    };
   
  return (
    <View>
      <Text>Vezi program</Text>
        {show && ( <DateTimePicker  testID="dateTimePicker" value={date} mode={mode} is24Hour={true} display="default" onChange={onChange}/>)} 
      <SubmitBtn onPress={showDatepicker}>
                 <SubmitBtnText>Alege datele</SubmitBtnText>
               </SubmitBtn> 
           <MultiSelect
                   
                    data={data}
                    labelField="label"
                    valueField="value"
                    label="Multi Select"
                    placeholder="Selecteaza ora"
                    search
                    searchPlaceholder="Search"
                    value={hour}
                    onChange={item => {
                    setHour(item);
                        console.log('hour', item);
                    }}
                  renderItem={item => _renderItem(item)}   />


<TouchableOpacity  onPress={addprogramm}>
          <Text >Adauga</Text>
          </TouchableOpacity>
    </View>
  );
};

export default index;
