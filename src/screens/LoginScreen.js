import React, {useContext, useState} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet,ScrollView} from 'react-native';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { AuthContext } from '../navigation/AuthProvider';
import SocialButton from '../components/SocialButton';
import ForgotPassword from './ForgotPassword';
import { Formik } from 'formik'
import * as yup from 'yup'
const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const {login, googleLogin} = useContext(AuthContext);

  return (
    <Formik
        initialValues={{ 
          name: '',
          email: '', 
          password: '' 
        }}
        onSubmit={values => Alert.alert(JSON.stringify(values))}
        validationSchema={yup.object().shape({
          email: yup
            .string()
            .email()
            .required('Vă rugăm să introduceţi adresa de email!'),
          password: yup
            .string()
            .min(4,'Parola trebuie sa fie min 4 caractere.')
            .required('Vă rugăm să introduceţi parola!'),
        })}
      
      >
         {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/login.jpg')}
        style={styles.logo}
      />
      <Text style={styles.text}>Programează-te online</Text>
  
      <FormInput
        labelValue={email}
        onChangeText={(userEmail) => setEmail(userEmail)}
        placeholderText="Email"
        iconType="user"
        onBlur={() => setFieldTouched('email')}
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
        
      />
     {touched.email && errors.email &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.email}</Text>
            }

      <FormInput
        labelValue={password}
        onChangeText={(userPassword) => setPassword(userPassword)}
        placeholderText="Parola"
        iconType="lock"
        secureTextEntry={true}
        onBlur={() => setFieldTouched('password')}
      />
    {touched.password && errors.password &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.password}</Text>
            }
      <FormButton
        buttonTitle="Autentificare"
        onPress={() => login(email, password)}
       
      />
        <SocialButton 
        buttonTitle="Autentifică-te cu Google"
        btnType="google"
        color="#de4d41"
        backgroundColor="#f5e7ea"
        onPress={() => googleLogin()}
      />
      <TouchableOpacity style={styles.forgotButton} onPress={() => {}}>
        <Text style={styles.navButtonText}  onPress={() => navigation.navigate('ForgotPassword')}>Ai uitat parola?</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.forgotButton}
        onPress={() => navigation.navigate('Signup')}>
        <Text style={styles.navButtonText}>
          Nu ai cont? Creează-ţi unul aici!
        </Text>
      </TouchableOpacity>
      </View>
      )}
      </Formik>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#fff6ff',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    height: 110,
    width: 150,
    resizeMode: 'cover',
    marginBottom: 10,
    marginTop: 30,
    borderRadius:10,
    
  },
  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 28,
    marginBottom: 10,
    color: '#940094',
  },
  navButton: {
    marginTop: 15,
  },
  forgotButton: {
    marginVertical: 35,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#940094',
    fontFamily: 'Lato-Regular',
  },
});