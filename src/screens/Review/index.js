import {  Text,StyleSheet, ActivityIndicator} from 'react-native';
import React,{useState,useContext} from 'react';
import firestore from '@react-native-firebase/firestore';
import { ScrollView} from 'react-native-gesture-handler';
import { Rating } from 'react-native-elements';
import storage from '@react-native-firebase/storage'
import { AuthContext } from '../../navigation/AuthProvider';
import ImagePicker from 'react-native-image-crop-picker';
import { TextInput,Button ,Card} from 'react-native-paper';
import {

  InputWrapper,
  AddImage,
  SubmitBtn,
  SubmitBtnText,
  StatusWrapper,
} from '../../styles/AddPost';
import { ScreenStackHeaderBackButtonImage } from 'react-native-screens';

const Review = ({route}) => {
  const {user}=useContext(AuthContext);
     const {nameSalon}=route.params
     const [comment, setComment]=useState(null)
    const [image, setImage]=useState(null)
     const[star, setStar]=useState(null)
    const [uploading, setUploading] = useState(false);
    const [transferred, setTransferred] = useState(0);
    const [name,setName]=useState(null);
  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      width: 1200,
      height: 780,
      cropping: true,
    }).then((image) => {
      console.log(image);
      const imageUri = image.path;
      setImage(imageUri);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 800,
      height: 780,
      cropping: true,
    }).then((image) => {
      console.log(image);
      const imageUri = image.path;
      setImage(imageUri);
    });
  };
 
  const uploadImage = async () => {
    if( image == null ) {
      return null;
    }
    const uploadUri = image;
    let filename = uploadUri.substring(uploadUri.lastIndexOf('/') + 1);
    const extension = filename.split('.').pop(); 
    const name = filename.split('.').slice(0, -1).join('.');
    filename = name + Date.now() + '.' + extension;

    setUploading(true);
    setTransferred(0);

    const storageRef = storage().ref(`photos/${filename}`);
    const task = storageRef.putFile(uploadUri);

    task.on('state_changed', (taskSnapshot) => {
      console.log(
        `${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`,
      );

      setTransferred(
        Math.round(taskSnapshot.bytesTransferred / taskSnapshot.totalBytes) *
          100,
      );
    });

    try {
      await task;

      const url = await storageRef.getDownloadURL();

      setUploading(false);
      setImage(null);
      return url;

    } catch (e) {
      console.log(e);
      return null;
    }

  };
  

    const submitPost = async () =>{
      const imageUrl = await uploadImage();
        console.log('Image Url: ', imageUrl);
        firestore()
  .collection('reviews')
  .add({
    userId:user.uid,
    name:nameSalon,
    namePerson:name,
    comment,
    img: imageUrl,
    time: firestore.Timestamp.fromDate(new Date()),
  })
  .then(() => {
    console.log('Review added!');
    
  })
  .catch((error)=>{ console.log('Ceva nu merge ok!')}) }

  return (
    <ScrollView style={styles.container}>
             
             <Text style={styles.title}> Cum evaluezi {nameSalon}?</Text> 
            
             <Card style={styles.card}>
             <TextInput label="Nume" style={styles.name} value={name}  onChangeText={(name) => setName(name)}/>
      <TextInput label="Adaugă un comentariu" style={styles.comment} value={comment}  onChangeText={(comment) => setComment(comment)}/>
          <InputWrapper>
        {image != null ? <AddImage source={{uri: image}} /> : null}

        <Button icon='camera' onPress={takePhotoFromCamera}>Deschide camera</Button>
          <Button icon='folder'  onPress={choosePhotoFromLibrary}>Alege o imagine</Button>
          {uploading ? (
          <StatusWrapper>
            <Text>{transferred} % Completed!</Text>
            <ActivityIndicator size="large" color="#0000ff" />
          </StatusWrapper>
        ) : (
          <SubmitBtn style={{margin:10}} onPress={submitPost}>
            <SubmitBtnText >Adauga</SubmitBtnText>
          </SubmitBtn>
        )}
      </InputWrapper> 
     
          </Card>
    </ScrollView>
  );
};

export default Review;


const styles=StyleSheet.create({
    container :{
        margin:0,
        backgroundColor:'#fff6ff', height:'100%'
    },
    title:{
        fontSize:16,
        fontWeight:'bold',
        margin:10,
        alignSelf:'center'

        
    },
    card:{
        margin:15,
        borderRadius:10,
        padding:15


    },
    comment :{
        margin:20,
        height:100,
         borderColor:'purple',
         color:'white'
        

    },
    name: {
      margin:20,
    }
})