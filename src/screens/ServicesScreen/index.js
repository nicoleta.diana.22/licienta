import React,{useState,useEffect} from 'react'
import {View,  SafeAreaView, ScrollView, ImageBackgrund,Image,FlatList} from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import Carousel from 'react-native-snap-carousel';

import BannerSlider from '../../components/BannerSlider/BannerSlider';
import CustomSwitch from '../../components/CustomSwitch';
import { servicesData } from '../../model/data';
import { windowWidth } from '../../utils/Dimension';
import DropDownPicker from 'react-native-dropdown-picker';
import {useNavigation} from '@react-navigation/native'; 

import {Button, Text,Card} from 'react-native-paper'


export default function Services () {
    const [servicesTab, setServicesTab]=useState(1)
    const navigation=useNavigation();
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [city, setCity] = useState([
      {label: 'Bucuresti', value:'Bucuresti'},
      {label: 'Iasi', value:'Iasi'},
      {label: 'Brasov', value:'Brasov'},
      {label: 'Cluj', value:'Cluj'},
      {label: 'Constanta', value:'Constanta'},
      {label: 'Craiova', value:'Craiova'},
      {label: 'Galati', value:'Galati'},
      {label: 'Ploiesti', value:'Ploiesti'},
      {label: 'Timisoara', value:'Timisoara'},
      {label: 'Oradea', value:'Oradea'},
      {label: 'Braila', value:'Braila'},
      {label: 'Arad', value:'Arad'},
      {label: 'Pitesti', value:'Pitesti'},
      {label: 'Sibiu', value:'Sibiu'},
      {label: 'Bacau', value:'Bacau'},
      {label: 'Tg Mures', value:'Tg Mures'},
      {label: 'Baia Mare', value:'BaiaMare'},
      {label: 'Buzau', value:'Buzau'},
      {label: 'Botosani', value:'Botosani'},
      {label: 'Suceava', value:'Suceava'},
      {label: 'Piatra Neamt', value:'PiatraNeamt'},
      {label: 'Vaslui', value:'Vaslui'},
      
      
    ]);
  
    const items = [
        {
          image: require("../../../assets/categories/barber2.jpg"),
          text: "Barber",
        },
        {
          image: require("../../../assets/categories/nails2.jpg"),
          text: "Unghii",
        },
        {
          image: require("../../../assets/categories/makeup2.jpg"),
          text: "Machiaj",
        },
        {
          image: require("../../../assets/categories/coafor.jpg"),
          text: "Coafor",
        },
        {
          image: require("../../../assets/categories/cosmetica.jpg"),
          text: "Cosmetica",
        },
        {
          image: require("../../../assets/categories/vopsit.jpg"),
          text: "Vopsit",
        },
        {
          image: require("../../../assets/categories/sprancene.jpg"),
          text: "Sprancene",
        },
        {
          image: require("../../../assets/categories/gene.jpg"),
          text: "Gene",
        },
        {
          image: require("../../../assets/categories/epilare.jpg"),
          text: "Epilare",
        },
        {
          image: require("../../../assets/categories/frizerie.jpg"),
          text: "Frizerie",
        },
        {
          image: require("../../../assets/categories/masaj.jpg"),
          text: "Masaj",
        },
        {
          image: require("../../../assets/categories/slabire.jpg"),
          text: "Slabire",
        },
        {
          image: require("../../../assets/categories/copii.jpg"),
          text: "Copii",
        },
      ];
      
    const renderBanner= ({item, index})=>{
       return <BannerSlider data={item}/>
    }

    const onSelectSwitch =(value) => {
      setServicesTab(value);
   
    }
   
    return (
        <SafeAreaView style={{flex:1,backgroundColor:'#fff6ff'}}>
            <ScrollView style={{padding:20}}>
                <Carousel
              // ref={(c) => { this._carousel = c; }}
              data={servicesData}
              renderItem={renderBanner}
              sliderWidth={windowWidth-40}
              itemWidth={300}
              loop={true}
            />
            <View style={{marginTop:20,borderRadius:20,marginVertical:15}} >
                <CustomSwitch selectionMode={1} option1="Saloane" option2="Servicii" 
                onSelectSwitch={onSelectSwitch}/>
            </View>

            {servicesTab ==1 &&
            <View style={{marginVertical:10, padding:0, backgroundColor:'#D8BFD8',borderRadius:10}}>
             <Text style={{margin:10,fontSize:14}}>Alege un oraş</Text>
             <DropDownPicker
      open={open}
      value={value}
      items={city}
      setOpen={setOpen}
      setValue={setValue}
      setCity={setCity}
      scrollViewProps={true}
      labelProps={{
        numberOfLines: 1
      }}
      max={10}
      maxHeight={200}
      disableBorderRadius={true}
      autoScroll={false}
      style={{
        backgroundColor: "white",
        margin:10,
        padding:5,
        width:340
      }}
    />
              
                  <Button   onPress={() => navigation.navigate('SearchResults',{value:value})}>Caută</Button>
             </View>
           
             }

            {servicesTab ==2 && 
           <View style={{marginVertical:15, padding:0, backgroundColor:'#D8BFD8',borderRadius:10}}>
            <Text style={{margin:10,fontSize:14}}>Alege un oraş</Text>
            <Card>
         
            <DropDownPicker
      open={open}
      value={value}
      items={city}
      setOpen={setOpen}
      setValue={setValue}
      setCity={setCity}
      autoScroll={true}
      style={{
        backgroundColor: "white",
        margin:10,
        padding:5,
        width:340
      }}
    />
        <Text style={{margin:10,fontSize:14}}>Alege un serviciu</Text>
        <View
      style={{
        margin: 10,
        backgroundColor: "#fff",
        paddingVertical: 10,
        paddingHorizontal:10,
        paddingLeft: 20 ,backgroundColor:'#fbf2ff',
        borderRadius:10,
      }}
    >
      <ScrollView horizontal showsHorizontalScrollIndicator={true}>
        {items.map((item, index) => (
          <TouchableOpacity onPress={() => navigation.navigate('Services',{text:item.text, value:value})}>
          <View key={index} style={{ alignItems: "center", marginRight: 30 }}>
          <Text style={{ fontSize: 13, fontWeight: "900" }}>{item.text}</Text>
            <Image
              source={item.image}
              style={{
                width: 50,
                height: 40,
                resizeMode: "contain"
             
              }}
            />
            
          </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
        </Card>
            </View>}

            
            <View style={{marginVertical:30, flexDirection:'column',backgroundColor:'#f3f3f3', padding:5, borderRadius:10,marginBottom:10, justifyContent:'space-between',marginTop:150}}>
            
                <Text style={{fontSize:16, fontFamily: 'Roboto-Medium', margin:10}}>Beneficii</Text>

                <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image source={require('../../../assets/images/beneficii1.jpg')}  style={{width: 70, height: 70, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 220}}>
                      <Text numberOfLines={1} style={{fontSize:14, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>Descoperă stilistul potrivit</Text>
                      <Text style={{fontSize:12, fontFamily: 'Roboto-Medium', margin:6}}>Inspiră-te din imagini si alege ceva pe gustul tău.</Text>
                     </View>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image source={require('../../../assets/images/beneficii2.jpg')}  style={{width: 70, height: 70, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 220}}>
                      <Text numberOfLines={1} style={{fontSize:14, fontFamily: 'Roboto-Medium' ,margin:6,  fontWeight:'bold'}}>Economiseste timp</Text>
                      <Text style={{fontSize:12, fontFamily: 'Roboto-Medium', margin:6}}>Programează-te în câteva minute la salonul ales şi reduce timpul pierdut.</Text>
                     </View>
                </View>
            </View>


            </ScrollView>
        </SafeAreaView>
    )
}

