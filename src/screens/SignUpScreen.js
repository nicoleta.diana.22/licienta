import React, {useContext, useState} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { AuthContext } from '../navigation/AuthProvider';
import SocialButton from '../components/SocialButton';
import * as yup from 'yup'
import { Formik } from 'formik'
const SignupScreen = ({navigation}) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const [name, setName]=useState();
  const {register} = useContext(AuthContext);
  const validationSchema = yup.object().shape({
    email: yup.string()
      .label('Email')
      .email('Enter a valid email')
      .required('Please enter a registered email'),
    password: yup.string()
      .label('Password')
      .required()
      .min(4, 'Password must have at least 4 characters ')
  })
  return (
    <Formik
    initialValues={{ 
      name: '',
      email: '', 
      password: '' 
    }}
    onSubmit={values => Alert.alert(JSON.stringify(values))}
    validationSchema={yup.object().shape({
      name: yup
        .string()
        .required('Vă rugăm să introduceţi numele!'),
      email: yup
        .string()
        .email()
        .required('Vă rugăm să introduceţi o adresă de email!'),
      password: yup
        .string()
        .min(4,'Vă rugăm să introduceţi minim 4 caractere!')
        .required(),
    })}
   >
        {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
    <View style={styles.container}>
      <Text style={styles.text}>Creează un cont</Text>
      <FormInput
        labelValue={name}
        onChangeText={(name) => setName(name)}
        placeholderText="Nume"
        iconType="user"
        autoCapitalize
        onBlur={() => setFieldTouched('name')}
      />
      {touched.name && errors.name &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.name}</Text>
            } 
      <FormInput
        labelValue={email}
        onChangeText={(userEmail) => setEmail(userEmail)}
        placeholderText="Email"
        iconType="user"
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
        onBlur={() => setFieldTouched('email')}
      />
{touched.email && errors.email &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.email}</Text>
            }
      <FormInput
        labelValue={password}
        onChangeText={(userPassword) => setPassword(userPassword)}
        placeholderText="Parolă"
        iconType="lock"
        secureTextEntry={true}
        onBlur={() => setFieldTouched('password')}
      />
       {touched.password && errors.password &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.password}</Text>
            }
      <FormInput
        labelValue={confirmPassword}
        onChangeText={(userPassword) => setPassword(userPassword)}
        placeholderText="Confirmă Parola"
        iconType="lock"
        secureTextEntry={true}
        onBlur={() => setFieldTouched('password')}
      />
 {touched.password && errors.password &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.password}</Text>
            }
<FormButton
        buttonTitle="Înregistrare"
        onPress={() => register(email, password,name)}
      />
      
        <SocialButton 
        buttonTitle="Înregistrare cu  Google"
        btnType="google"
        color="#de4d41"
        backgroundColor="#f5e7ea"
        onPress={() => {}}
      />
      <View style={styles.textPrivate}>
        <Text style={styles.color_textPrivate}>Prin înregistrare confirmaţi că acceptaţi </Text>
        <TouchableOpacity onPress={() => alert('Terms Clicked!')}>
          <Text style={[styles.color_textPrivate, {color: '#e88832'}]}>Termenii</Text>
        </TouchableOpacity>
        <Text style={styles.color_textPrivate}> şi </Text>
        <Text style={[styles.color_textPrivate, {color: '#e88832'}]}>Politica de Confidenţialitate</Text>
      </View>

      <TouchableOpacity
        style={styles.navButton}
        onPress={() => navigation.navigate('Login')}>
        <Text style={styles.navButtonText}>
          Ai deja un cont? Autentifica-te
        </Text>
      </TouchableOpacity>
    </View>
     )}
    </Formik>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#fff6ff',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 28,
    marginBottom: 10,
    color: '#940094',
  },
  navButton: {
    marginTop: 15,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#940094',
    fontFamily: 'Lato-Regular',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 35,
    justifyContent: 'center',
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: '400',
    fontFamily: 'Lato-Regular',
    color: 'grey',
  },
});