import {SafeAreaView,Text,View,StyleSheet,  Share} from 'react-native';
import React, {useState,useRef} from 'react';
import QRCode from 'react-native-qrcode-svg';
import {Button, Card} from 'react-native-paper'
const index = ({route, navigation}) => {
    const {nameSalon}=route.params
    const {service}=route.params
    const {name}=route.params
    const {name2}=route.params
    const {hour}=route.params
    const {date}=route.params
    const [inputText, setInputText] = useState('');
  const [qrvalue, setQrvalue] = useState('');
  let myQRCode = useRef();
  const shareQRCode = () => {
    myQRCode.toDataURL((dataURL) => {
      console.log(dataURL);
      let shareImageBase64 = {
        title: 'Programare',
        url: `data:image/png;base64,${dataURL}`,
        subject: 'Share Link',
      };
      Share.share(shareImageBase64).catch((error) => console.log(error));
    });
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <Text style={styles.titleStyle}>
         Generează codul cu informaţiile tale
        </Text>
        <QRCode
          getRef={(ref) => (myQRCode = ref)}
          value={qrvalue ? qrvalue : 'Exista o eroare!'}
          size={250}
          color="black"
          backgroundColor="white"
          logoSize={30}
          logoMargin={2}
          logoBorderRadius={15}
          logoBackgroundColor="yellow"
        />
       
        <Card.Actions>
        <Button  onPress={() => setQrvalue('Salon: ' + nameSalon+ ', '+'Serviciu: '+service+ ',' + 'Angajat:  ' + name + ' '+ name2 + ',' + 'data: '+ date + ','+'ora:'+hour)} >Generează codul</Button>
     
        <Button icon='share' onPress={shareQRCode} >Distribuie codul</Button>
        </Card.Actions>
       
      </View>
    </SafeAreaView>
  );
};
export default index;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff6ff',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 20,
  },
  titleStyle: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
  },
  textStyle: {
    textAlign: 'center',
    margin: 10,
  },
  textInputStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#51D8C7',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#51D8C7',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 30,
    padding: 10,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
});