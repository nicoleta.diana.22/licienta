import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
 import Fontisto from 'react-native-vector-icons/Fontisto';
import ProfileScreen from '../screens/ProfileScreen'
import LocationSearchScreen from '../screens/LocationSearchScreen';
import SearchResults from '../screens/SearchResults';
import PostScreen from '../screens/PostScreen'
import ServicesScreen from '../screens/ServicesScreen'
import SearchStilist from '../screens/SearchStilist';
import SearchSalon from '../screens/SearchSalon'
import BookingScreen from '../screens/BookingScreen'
import Activity from '../screens/Activity'
import Review from '../screens/Review'
import ViewOnMap from '../screens/ViewOnMap'
import Onboarding from '../screens/Onboarding';
import Services from '../screens/Services'
import ViewMyReviews from '../screens/ViewMyReviews'
import QRInfoScreen from '../screens/QRInfoScreen'
import {NavigationContainer} from '@react-navigation/native';
import { createDrawerNavigator } from "@react-navigation/drawer";
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const HomeStack = ({navigation}) => (
  <Stack.Navigator >
    <Stack.Group screenOptions={{ headerStyle: {  backgroundColor:'#F5F5F5' } }}>
    <Stack.Screen name="Exploreaza" component={HomeScreen}  options={{ title: "Explorează"}} />
    <Stack.Screen
      name="Location"
      component={LocationSearchScreen}
      options={{
        title: "Caută o locaţie ..."}}
    />
    <Stack.Screen
      name="SearchResults"
      component={SearchResults}
      options={{
        title: "SearchResults"}}
     />
        <Stack.Screen
      name="SearchStilist"
      component={SearchStilist}
      options={{
        title: "SearchStilist"}}
     />
          <Stack.Screen
      name="SearchSalon"
      component={SearchSalon}
      options={{
        title: "SearchSalon"}}
     />
      <Stack.Screen
      name="PostScreen"
      component={PostScreen}
      options={{
        title: "Salon"}}
     />
       <Stack.Screen
      name="BookingScreen"
      component={BookingScreen}
      options={{
        title: "Programare"}}
    />
       <Stack.Screen
      name="Activity"
      component={Activity}
      options={{
        title: "Activity"}}
    />
    </Stack.Group>
  </Stack.Navigator>
);
const ProfilStack = ({navigation}) => (
  <Stack.Navigator >
      <Stack.Group screenOptions={{ headerStyle: {  backgroundColor:'#F5F5F5' } }}>
    <Stack.Screen name="ProfilScreen" component={ProfileScreen}  options={{
        title: "Profilul meu"}}/>
   </Stack.Group >
  </Stack.Navigator>
);

const ServicesStack = ({navigation}) => (
  <Stack.Navigator>
    <Stack.Group screenOptions={{ headerStyle: {  backgroundColor:'#F5F5F5' } }}>
     <Stack.Screen
      name="ServicesScreen"
      component={ServicesScreen}
      options={{
        title: "Caută un serviciu"}}
    />
        <Stack.Screen
      name="SearchResults"
      component={SearchResults}
      options={{
        title: "Saloane"}}
       
    />
    
    <Stack.Screen
      name="Services"
      component={Services}
      options={{
        title: "Services"}}
       
    />
    </Stack.Group>
    
  </Stack.Navigator>
);

const ActivityStack = ({navigation}) => (
  <Stack.Navigator>
     <Stack.Group screenOptions={{ headerStyle: {  backgroundColor:'#F5F5F5' } }}>
     <Stack.Screen
      name="Activity"
      component={Activity}
      options={{
        title: "Programările mele"}}
    />
        <Stack.Screen
      name="ViewOnMap"
      component={ViewOnMap}
      options={{
        title: "Harta"}}
       
    />
      <Stack.Screen
      name="Review"
      component={Review}
      options={{
        title: "Adaugă o recenzie"}}
       
    />
     <Stack.Screen
      name="ViewMyReviews"
      component={ViewMyReviews}
      options={{
        title: "Recenziile mele"}}
       
    />
     <Stack.Screen
      name="QRInfoScreen"
      component={QRInfoScreen}
      options={{
        title: "Scanează QR"}}
       
    />
    </Stack.Group>
  </Stack.Navigator>
);

const AppStack = () => {
  const getTabBarVisibility = (route) => {
    const routeName = route.state
      ? route.state.routes[route.state.index].name
      : '';

    if (routeName === 'Chat') {
      return false;
    }
    if (routeName === 'Mesaje') {
      return false;
    }
    return true;
  };
return (

  <Tab.Navigator  screenOptions={{ headerShown: false}} >
    <Tab.Screen
      name="Explorează" 
      component={HomeStack}
      options={({route}) => ({
        tabBarActiveTintColor: '#8A2BE2',
        tabBarIcon: ({color, size}) => (
          <Fontisto name="search" size={25} color={color} />
        ),
      })}
    />
   
      <Tab.Screen
      name="ServiciiStack"
      component={ServicesStack}
      options={{
        title:'Servicii',
        tabBarActiveTintColor: '#8A2BE2',
        tabBarIcon: ({color, size}) => (
          <Ionicons name="settings-outline" color={color} size={size} />
        ),
      }}
    />
     <Tab.Screen style={styles.tab}
      name="ActivityStack" 
      component={ActivityStack}
      options={({route}) => ({
        tabBarActiveTintColor: '#8A2BE2',
        title:'Programări',
        tabBarIcon: ({color, size}) => (
          <Fontisto name="clock" size={25} color={color} />
        ),
      })}
    />

    <Tab.Screen
      name="Profile"
      component={ProfilStack}
      options={{
        title:'Profil',
        tabBarActiveTintColor: '#8A2BE2',
        tabBarIcon: ({color, size}) => (
          <Ionicons name="person-outline" color={color} size={size} />

        ),
      }}
    />

  </Tab.Navigator>

);
}

export default AppStack;

const styles = StyleSheet.create({
text :{
  color:'#239991',
  fontSize:25,
  fontWeight:'bold'

},
tab:{
  backgroundColor:'#E6E6FA'
}

  
});