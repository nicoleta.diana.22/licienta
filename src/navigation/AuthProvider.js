import React, {createContext, useState} from 'react';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { GoogleSignin } from '@react-native-community/google-signin';
import { Alert } from 'react-native';
export const AuthContext = createContext();
export const AuthProvider = ({children}) => {
    const [user, setUser] = useState(null);
  
    return (
      <AuthContext.Provider
        value={{
          user,
          setUser,
          login: async (email, password) => {
            try {
              await auth().signInWithEmailAndPassword(email, password);
              
            } catch (e) {
              console.error('Email sau parolă greşită! ');
             
            }
          },
          register: async (email, password,name) => {
            try {
              await auth().createUserWithEmailAndPassword(email, password,name).then(()=>{
                firestore().collection("users").doc(auth().currentUser.uid).set({
                  uid: auth().currentUser.uid,
                  name,
                  email
              })
              })
            } catch (e) {
              console.error('Acest email exista!');
            }
          },
          logout: async () => {
            try {
              await auth().signOut();
            } catch (e) {
              console.log(e);
            }
          },
          googleLogin: async () => {
            const { idToken } = await GoogleSignin.signIn();
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);
            return auth().signInWithCredential(googleCredential);
          }, 
          passwordReset: email => {
            return auth().sendPasswordResetEmail(email)
          },
          
          
        }}>
        {children}
      </AuthContext.Provider>
    );
  };