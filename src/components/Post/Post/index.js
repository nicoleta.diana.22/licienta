import React from 'react';
import {View, Text, Image, Pressable,StyleSheet} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';

const Post = ({item}) => {

  const navigation = useNavigation();
  
  return (
    
    <Pressable  style={styles.container} onPress={() => navigation.navigate('PostScreen',{item}) }>
      <Card style={styles.card}>
      <Image  style={styles.image} source={{uri: item.img}}/>
      <Text style={styles.textname}>{item.name} </Text>
      <Text style={styles.textaddress}>{item.address}</Text>

      <Button > Vizualizeza</Button>
      </Card>
    </Pressable>
  );
};

export default Post;

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  image: {
    width: '100%',
    aspectRatio: 5 / 2,
    resizeMode: 'cover',
    borderRadius: 30,
   
    
  },
  card:{
   margin:5,
   borderRadius: 30,
  },
  textname:{
    fontSize:20,
    fontWeight:'bold',
    margin:15,
  },
  textaddress:{
    margin:8,
    fontSize:15,
  }




  
});
