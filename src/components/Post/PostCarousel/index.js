import React from 'react';
import { View, Text, Image, useWindowDimensions, Pressable } from "react-native";
import styles from './styles.js';
import { useNavigation } from '@react-navigation/native';

const Post = ({item}) => {

  const width = useWindowDimensions().width;

  const navigation = useNavigation();

  // const goToPostPage = () => {
  //   navigation.navigate('Post', {postId: post.id});
  // }

  return (
    <Pressable  style={[styles.container, { width: width - 60}]} onPress={() => navigation.navigate('PostScreen',{item})}>
      <View style={styles.innerContainer}>
      
        <Image
          style={styles.image}
          source={{uri: item.img}}
        />

        <View style={{flex: 1, marginHorizontal: 10}}>
     
          <Text style={styles.name}>
            {item.name}
          </Text>

          <Text style={styles.address} numberOfLines={2}>
           {item.address}
          </Text>

         
        </View>
      </View>
    </Pressable>
  );
};

export default Post;