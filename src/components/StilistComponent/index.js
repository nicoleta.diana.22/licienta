import { View, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import {Card,Text,Title,Paragraph,Button} from 'react-native-paper'


const index = ({item}) => {
  return (
    <View style={styles.container}>
        <TouchableOpacity>
     <Card style={styles.card}>
       <View style={styles.viewname}>
         <Text style={styles.name}>{item.staffFname} {item.staffLname}</Text>
       </View>
        <Button>Vezi servicii</Button>

      </Card>
      </TouchableOpacity>
    </View>
  );
};

export default index;

const styles=StyleSheet.create({
  container:{
      margin:8,
      
      
  },
  name:{
    fontSize:20,
    margin:10,
    padding:5,
  },
  card:{
      padding:10,
      justifyContent:'center',
      
  },
  services:{

  },
  viewname:{
      backgroundColor:'#DCDCDC'
  },
  cardcontent:{
      margin:10,
      backgroundColor:'#ffefd5'
  },
  textcontact:{
      fontSize:15,
      fontWeight:'bold'
  },
  viewinformatii:{
      margin:10
  }

})