import { View, Text,StyleSheet,Image,Alert} from 'react-native'
import React,{useState}from 'react'
import {Card,Title,Paragraph,Button} from 'react-native-paper'
import { windowWidth } from '../../utils/Dimension';
import { useNavigation } from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
const index = ({item,props}) => {
  const navigation = useNavigation();
  const[deleted, setDeleted]=useState()

const deleteReview= () => {
  firestore()
    .collection('reviews').doc(`${item.id}`)
    .delete()
    .then(() => {
      console.log('S-a sters')
      setDeleted(true);
    })
    .catch((e) => console.log('Error', e));
};
const handleDelete = (id) => {
      Alert.alert(
    'Sterge acest comentariu',
    'Esti sigura?',
    [
      {
        text: 'Renunta',
        onPress: () => console.log('Ai renuntat!'),
        style: 'cancel',
      },
      {
        text: 'Confirma',
        onPress: () => deleteReview(),
      },
    ],
    {cancelable: false},
  );
};
  return (
     <Card style={styles.card} >
       <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image  source={{uri: item.img}}  style={{width: 80, height: 80, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 180}}>
                      <Text numberOfLines={1} style={{fontSize:18, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>{item.name}</Text>
                      <Text style={{fontSize:14, fontFamily: 'Roboto-Medium', margin:6}}>{item.comment} </Text>
                      <Button icon="delete" onPress={handleDelete}>Sterge</Button>
                     </View>
        </View>
     </Card>
  
  )
}

export default index

const styles=StyleSheet.create({
    image:{

    },
    card:{
        padding:5,
        margin:15
    }
})