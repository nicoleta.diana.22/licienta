import { View, Text,StyleSheet,Image,ScrollView} from 'react-native'
import React from 'react'
import {Card,Title,Paragraph,Button} from 'react-native-paper'
import { windowWidth } from '../../utils/Dimension';
import { useNavigation } from '@react-navigation/native';
const index = ({item,props}) => {
  const navigation = useNavigation();
  return (
    <ScrollView >
     <Card style={styles.card} >
       <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:6}}>
                      <Image  source={{uri: item.img}}  style={{width: 80, height: 80, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 180}}>
                      <Text numberOfLines={1} style={{fontSize:18, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>{item.staffFname} {item.staffLname}</Text>
                      <Text style={{fontSize:14, fontFamily: 'Roboto-Medium', margin:6}}>{item.service} </Text>
                      <Text style={{fontSize:14, fontFamily: 'Roboto-Medium', margin:6}}>{item.price} lei</Text>
                      
                     </View>
        </View>
        <Card.Actions>
                    <Button onPress={() => navigation.navigate('BookingScreen',{item}) }> Programează-te</Button>
         </Card.Actions>
        
     </Card>
     </ScrollView>
  
  )
}

export default index

const styles=StyleSheet.create({
    image:{

    },
    card:{
        padding:5,
        margin:15
    }
})