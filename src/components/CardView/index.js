import React from 'react';
import ImagedCardView from "react-native-imaged-card-view";



const Services=() => {
    return (
        <ImagedCardView
        stars={5}
        reviews={456}
        ratings={4.5}
        title="Yosemite"
        rightSideValue="$990"
        subtitle="California"
        leftSideValue="3 Days"
        backgroundColor="#ff6460"
      />
    )
   
}



export default Services;