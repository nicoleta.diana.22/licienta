import { View, Text,Image } from 'react-native'
import React from 'react'
import {Card} from 'react-native-paper'
import Ionicons from 'react-native-vector-icons/Ionicons';
const index = ({item}) => {
  
  return (
    <View>
  <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:8, borderRadius:20, backgroundColor:'#EEE0E5', padding:15 }}>
  <Image source= {require('../../../assets/images/user.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}} />
    <Text style={{fontSize:16, fontFamily: 'Roboto-Medium', margin:10, fontColor:'#4B0082'}}> {item.name}</Text>
</View>
<View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:8, borderRadius:20, backgroundColor:'#EEE0E5', padding:15 }}>
  <Image source= {require('../../../assets/images/email.png')}  style={{width: 30, height: 30, borderRadius: 10, marginRight: 8}} />
    <Text style={{fontSize:16, fontFamily: 'Roboto-Medium', margin:10, fontColor:'#4B0082'}}> {item.email}</Text>
</View>
</View>
  )
}

export default index