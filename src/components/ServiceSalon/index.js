import { View, Text,StyleSheet,Image} from 'react-native'
import React from 'react'
import {Card,Title,Paragraph,Button} from 'react-native-paper'
import { windowWidth } from '../../utils/Dimension';
import { useNavigation } from '@react-navigation/native';
const index = ({item,props}) => {
  const navigation = useNavigation();
  return (
     <Card style={styles.card} >
       <View style={{flexDirection: 'row', alignItems: 'center', flex: 1,margin:2}}>
                      <Image  source={{uri: item.img}}  style={{width: 80, height: 80, borderRadius: 10, marginRight: 8}}  />     
                      <View style={{width: windowWidth - 180}}>
                      <Text numberOfLines={1} style={{fontSize:18, fontFamily: 'Roboto-Medium' ,margin:6, fontWeight:'bold'}}>{item.staffFname} {item.staffLname}</Text>
                      <Text style={{fontSize:14, fontFamily: 'Roboto-Medium', margin:6}}>{item.service} </Text>
                      <Text style={{fontSize:14, fontFamily: 'Roboto-Medium', margin:6}}>{item.price} lei</Text>
                      
                     </View>
        </View>
        <Card.Actions>
                    <Button onPress={() => navigation.navigate('BookingScreen',{item, address:item.address, city:item.city, latitude:item.latitude, longitude:item.longitude,name:item.name}) }> Programează-te</Button>
         </Card.Actions>
        
     </Card>
  
  )
}

export default index

const styles=StyleSheet.create({
    image:{

    },
    card:{
        padding:5,
        margin:5
    }
})