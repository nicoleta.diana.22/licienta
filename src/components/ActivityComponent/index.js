
import React,{useState,useEffect} from 'react'
import {Card,Avatar,Button,List,Colors} from 'react-native-paper'
import { View, Text, FlatList,StyleSheet,Modal,Alert,TouchableHighlight} from 'react-native'
import firestore from '@react-native-firebase/firestore';
import {useNavigation} from '@react-navigation/native';
import ViewOnMap from '../../screens/ViewOnMap'
const index = ({item}) => {
    const [deleted, setDeleted] = useState(false);
    const navigation = useNavigation();
    const [show,setShow]=useState(false)
    const [modalVisible, setModalVisible] = useState(false);
    const date=new Date().toLocaleDateString();
    const showModal = () => {
      setModalVisible(true);
      setTimeout(() => {
        setModalVisible(false);
      }, 5000);
    };
    const deleteActivity= () => {
        firestore()
          .collection('programari').doc(`${item.activityId}`)
          .delete()
          .then(() => {
            console.log('S-a anulat')
            setDeleted(true);
              showModal();
              setTimeout(() => {
                navigation.navigate('Exploreaza');
                }, 3000);
            
          
          })
          .catch((e) => console.log('Error', e));
      };
      const handleDelete = (activityId) => {
            Alert.alert(
          'Anulează programarea',
          'Esti sigur(ă)?',
          [
            {
              text: 'Renunţă',
              onPress: () => console.log('Ai renuntat!'),
              style: 'cancel',
              style:{color:'red'}
            },
            {
              text: 'Confirmă',
              onPress: () => deleteActivity(),
            

              
            },
          ],
          {cancelable: false},
        );
      };
      useEffect(() =>{
        if(item.date < date){
          setShow(true)
        }
        if(item.date > date){
          setShow(false)
        }
     },[]);       
     useEffect(() =>{
      if(item.date < date){
        setShow(true)
      }
      if(item.date > date){
        setShow(false)
      }
   },[]);  
   
  
  return (
    <View>

      <Card item={item} style={styles.card}>
             
                <Text style={styles.serviceText}>  {item.service} </Text>
                <View style={styles.categories}>
                <Text style={styles.textname}>{item.staffName} {item.staffLName} </Text>
                <Text style={styles.textprice}>  {item.price} RON  </Text>
                </View>
                    <View style={styles.categories}>
                     <List.Icon color={Colors.blue500} icon="calendar" label={item.services}/>
                     <Text style={styles.textinfo}> {item.date}</Text>
                    </View>
                    <View style={styles.categories}>
                    <List.Icon color={Colors.blue500} icon="clock" />
                    <Text style={styles.textinfo}>Ora:{item.hour}</Text>
                    </View>
                    <View style={styles.categories}>
                    <List.Icon color={Colors.blue500} icon="map" />
                    <Text style={styles.textinfo}>{item.address}, {item.city}</Text>
                    </View>
                    <View style={styles.categories}>
                    <List.Icon color={Colors.blue500} icon="pin" />
                    <Text style={styles.textinfo}>{item.name}</Text>
                    </View>
               <Card.Actions>
                <Button icon="delete" onPress={handleDelete}>ANULEAZĂ</Button>
                <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View
          style={{
            
            alignItems: 'center',
            backgroundColor: '#3CB371',
            justifyContent: 'center',
            borderRadius:10,
            height:100,
            width:350,
            alignSelf:'center',
            marginTop:200,
            opacity:1
          }}>
          <Text style={{fontSize: 16, color: 'white'}}>
            Programarea s-a anulat cu succes!
       
          </Text>
        </View>
      </Modal>
                <Button  icon="map" onPress={() => navigation.navigate('ViewOnMap',{latitude:item.latitude, longitude:item.longitude, address:item.address, city:item.city})}>Vezi locaţia</Button>
                </Card.Actions>
              
              <Button  icon='camera'  onPress={() => navigation.navigate('QRInfoScreen',{name:item.staffLName, name2:item.staffName,service:item.service, nameSalon:item.name,hour:item.hour,date:item.date})}>Deschide codul QR</Button>
              
           
               { show &&(
                 <View>
                   <Button icon='star' onPress={()=>navigation.navigate('Review',{nameSalon:item.name, Lname:item.staffLName,Fname:item.staffFname,services:item.service})}>Adaugă o recenzie</Button>
                 </View>
               )}
              
       
                
               
           </Card>
    </View>
  )
}

export default index
const styles=StyleSheet.create({

    card:{
        margin:5,
        padding:10,
        borderRadius:15,
        

    },
    serviceText:{
       fontSize:16,
       fontWeight:'bold',
       color:'#DDA0DD',
       marginBottom:10,
       justifyContent:'center',
       alignSelf:'center'

        
    },
    price:{
        marginRight:0,
    },
    categories:{
        flexDirection:'row'
    },
    textinfo:{
        marginTop:20,
        fontWeight:'bold',
        fontSize:14
    },
    textprice:{
        marginRight:20,
        
    },
    textname:{
      fontWeight:'bold',
      color:'blue'
    }
})