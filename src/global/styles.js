import styled from 'styled-components/native';
export const colors = {
    buttons:"#ff8c52",
    grey1: '#43484d',
    grey2: '#5e6977',
    grey3: '#86939e',
    grey4: '#bdc6cf',
    grey5: '#e1e8ee',
    CardComment : '#86939e',
    cardbackground:"white",
    statusbar:"#ff8c52",
    heaherText:"white",
    lightgreen: '#66DF48',
}


export const parameters ={
    headerHeight :40,

    styledButton:{
        backgroundColor:"#ff8c52",
        alignContent:"center",
        justifyContent:"center",
        borderRadius:12,
        borderWidth:1, 
        borderColor:"#ff8c52",
        height:50,
        paddingHorizontal:20,
        width:'100%'
    },

    buttonTitle:{
        color:"white",
        fontSize:20,  
        fontWeight:"bold" ,
        alignItems:"center",
        justifyContent:"center"  ,
        marginTop:-3 
    }
}

export const title ={
    color:"#ff8c52",
    fontSize :20,
    fontWeight:"bold"
}

export const  fonts ={
  
  android: {
    regular: 'Roboto',
    italic: 'Roboto-Italic',
    thin: 'Roboto-Thin',
    thinItalic: 'Roboto-ThinItalic',
    light: 'Roboto-Light',
    lightItalic: 'Roboto-LightItalic',
    medium: 'Roboto-Medium',
    mediumItalic: 'Roboto-MediumItalic',
    bold: 'Roboto-Bold',
    boldItalic: 'Roboto-BoldItalic',
    condensed: 'RobotoCondensed-Regular',
    condensedItalic: 'RobotoCondensed-Italic',
  }
}
export const BookNow = styled.View`
  flex-direction: row;
  align-items: center;
  flex: 1;
  justify-content: flex-end;
`

export const BookNowButton = styled.TouchableOpacity`
  align-items: center;
  background-color: #f4e22c;
  padding: 10px;
  border-radius: 20px;
  width: 100%;
  margin-left: auto;
`

export const ButtonText = styled.Text`
  font-weight: bold;
  font-size: 15px;
`