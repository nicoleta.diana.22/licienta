export default data = [
    {
        _id: '1',
        title: 'Caută un salon',
        description: 'Vezi saloanele din oraşul tău',
        img: require('../../assets/icons/onboarding/onb1.jpg')
    },
    {
        _id : '2',
        title: 'Ce serviciu cauţi?',
        description: 'Alege un serviciu.',
        img: require('../../assets/icons/onboarding/onb2.jpg')
    },
    {
      _id : '3',
      title: 'Alege un stilist',
      description: 'Vezi  stiliştii disponibili.',
      img: require('../../assets/icons/onboarding/onb3.jpg')
    },
    {
        _id : '4',
        title: 'Programează-te',
        description: 'Finalizează programarea la stilistul favorit.',
        img: require('../../assets/icons/onboarding/onb4.jpg')
      },
]